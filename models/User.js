let mongoose = require('mongoose');

const userSchema = mongoose.Schema({
	userName: {
		type: String,
		required: [true, "First name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderCarts: [
		{
			productId: {
				type: String,
				required: [true, "ProductId is required"]
			},
			name: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			imageUrl: {
				type: String,
				default: "https://placehold.co/600x400"
			},
			subTotal: {
				type: Number,
				default: 0
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);