let mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			name: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Product quantity is required"]
			},
			subTotal: {
				type: Number,
				required: [true, "Product total is required"]
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"]
	},
	paymentDetails: {
		type: String,
		default: ""
	},
	deliveryAddress: {
		type: String,
		default: ""
	},
	orderStatus: {
		type: String,
		default: "Pending"
	},
	purchasedOn: {
		type: Date
	}
})

module.exports = mongoose.model("Order", orderSchema);