const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

router.post('/register', (request, response) => {
	UserController.registerUser(request, response)
})

router.post('/login', (request, response) => {
	UserController.loginUser(request, response)
})

router.get('/logout', auth.verify, (request, response) => {
	response.send("Successfully logout!");
})

router.get("/details", auth.verify, (request, response) => {
  UserController.getProfile(request, response)
})

router.get("/details/all", auth.verify, auth.verifyAdmin, (request, response) => {
  UserController.getAllProfile(request, response)
})

router.get("/details/:id", auth.verify, auth.verifyAdmin, (request, response) => {
  UserController.getUserProfile(request, response)
})

router.put('/update', auth.verify, (request, response) => {
	UserController.updateUser(request, response)
})

router.put('/reset-password', auth.verify, (request, response) => {
	UserController.resetPassword(request, response)
})

router.put('/profile', auth.verify, (request, response) => {
	UserController.updateProfile(request, response)
})

router.put('/set-admin', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.setAdmin(request, response);
})

router.get('/get-cart', auth.verify, (request, response) => {
	UserController.getCart(request, response);
})

router.put('/edit-cart', auth.verify, (request, response) => {
	UserController.editCart(request, response);
})

router.get('/remove-cart/:id', auth.verify, (request, response) => {
	UserController.removeCart(request, response);
})

router.get('/clear-cart', auth.verify, (request, response) => {
	UserController.clearCart(request, response);
})

module.exports = router;