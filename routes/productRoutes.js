const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

const {verify, verifyAdmin} = auth;

router.post('/', verify, verifyAdmin, (request, response) => {
	ProductController.registerProduct(request, response);
});

router.get('/all', (request, response) => {
	ProductController.getAllProducts(request, response)
});

router.get('/active', (request, response) => {
	ProductController.getAllActiveProducts(request, response)
});

router.get('/archive', verify, verifyAdmin, (request, response) => {
	ProductController.getAllArchivedProducts(request, response)
});

router.get('/:id', (request, response) => {
	ProductController.getProductById(request, response)
});

router.post('/search', (request, response) => {
	ProductController.searchProductByName(request, response);
})

router.post('/searchByNameAndPrice', (request, response) => {
  ProductController.searchProductByNameAndPrice(request, response);
});

router.put('/:id', verify, verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response)
});

router.put("/:id/activate", verify, verifyAdmin, (request, response) => {
    ProductController.activateProduct(request, response);
  }
);

router.put("/:id/archive", verify, verifyAdmin, (request, response) => {
    ProductController.archiveProduct(request, response);
  }
);

router.post('/addToCart', verify, (request, response) => {
	UserController.addToCart(request, response);
});



module.exports = router;