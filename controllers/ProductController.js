const Product = require('../models/Product.js');

module.exports.registerProduct = (request, response) => {
	return Product.find({name: request.body.name}).then((result, error) => {
		if(error) return response.status(500).send({status: false, message: error.message})
		if(result.length > 0) return response.status(400).send({status: false, message:"Product Exists"});
		let newProduct = new Product({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			imageUrl: request.body.imageUrl
		})
		return newProduct.save().then((registered_product, error) => {
			if(error) return response.status(500).send({status: false, message: error.message})
			return response.status(201).send({status: true, message: "Successfully registered a product!", data: registered_product});
		})
	})
}

module.exports.getAllProducts = (request, response) => {
	return Product.find({}).then((result) => {
		return response.status(200).send(result);
	}).catch(error => response.status(500).send(error));
}

module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		let selectedProduct = result.map(product => {
		return {
			id: product._id,
			name: product.name,
			description: product.description,
			price: product.price,
			imageUrl: product.imageUrl
		}
		});
		return response.status(200).send(selectedProduct);
	}).catch(error => response.status(500).send(error));
}

module.exports.getAllArchivedProducts = (request, response) => {
	return Product.find({isActive: false}).then(result => {
		let selectedProduct = result.map(product => {
		return {
			Id: product._id,
			Name: product.name,
			Description: product.description,
			Price: product.price
		}
		});
		return response.status(200).send(selectedProduct);
	}).catch(error => response.status(500).send(error));
}

module.exports.getProductById = (request, response) => {
	return Product.findById(request.params.id).then(result => {
		return response.status(200).send(result);
	}).catch(error => response.status(500).send(error));
}

// If user is an admin, it returns all relevant products
// If user is not an admin, it returns all relevant and active products
// $regex checks if the product name contains a string equivalent to the input; $options: 'i' searches in lowercase
module.exports.searchProductByName = (request, response) => {
	return Product.find({name: { $regex: request.body.name, $options: 'i' }}).then((result, error) => {
		if(error) return response.status(500).send({message: error.message});
		if(result.length <= 0) return response.status(400).send({message: "Search not found"});
		
		let selectedProduct;
		if(request.user.isAdmin) {
			selectedProduct = result.map(product => {
			return {
				Id: product._id,
				Name: product.name,
				Description: product.description,
				Price: product.price,
				isActive: product.isActive
			}})
		}
		else {
			selectedProduct = result.filter(product => {
				return product.isActive;
			}).map(product => {
				return {
				Id: product._id,
				Name: product.name,
				Description: product.description,
				Price: product.price
			}})
		}
		
		return response.status(200).send(selectedProduct);
	})
}

module.exports.searchProductByNameAndPrice = (request, response) => {
  const { name, minPrice, maxPrice } = request.body;
  
  // Create a query object to build the search query dynamically
  const query = {isActive: true};

  if (name) {
    // Use a case-insensitive regex to search by name
    query.name = { $regex: name, $options: 'i' };
  }

  if ((minPrice !== null && minPrice !== undefined && minPrice !== '') && (maxPrice !== null && maxPrice !== undefined && maxPrice !== '')) {
    // Search within the specified price range
    query.price = { $gte: minPrice, $lte: maxPrice };
  } 
  else if ((minPrice !== null && minPrice !== undefined && minPrice !== '')) {
    // Search for products with price greater than or equal to minPrice
    query.price = { $gte: minPrice };
  } 
  else if (maxPrice !== null && maxPrice !== undefined && maxPrice !== '') {
    // Search for products with price less than or equal to maxPrice
    query.price = { $lte: maxPrice };
  }

  // Use the query object to find matching products
  Product.find(query)
    .then((result) => {
      return response.status(200).send({status: true, message: result});
    })
    .catch((error) => {
      return response.status(500).send({status: false, message: error.message });
    });
};

module.exports.updateProduct = (request, response) => {
	return Product.findById(request.params.id).then(result => {
		// Uses || to prevent a error of user only updating one of the fields below
		result.name = request.body.name || result.name;
		result.description = request.body.description || result.description;
		result.price = request.body.price || result.price;
		result.imageUrl = request.body.imageUrl || result.imageUrl;
		return result.save().then(updated_product => {
			return response.status(200).send({message: "Product has been updated successfully", data: updated_product});
		}).catch(error => {
				return response.status(500).send({message: error.message});
	})
	}).catch(error => {
			return response.status(500).send({message: error.message});
	})
}

module.exports.activateProduct = (request, response) => {
  return Product.findByIdAndUpdate(request.params.id, {
    $set: {
      isActive: true,
    },
  }).then((result, error) => {
    if (error) return response.status(500).send({ message: error.message });
    return response.status(200).send(true);
  });
};

module.exports.archiveProduct = (request, response) => {
  return Product.findByIdAndUpdate(request.params.id, {
    $set: {
      isActive: false,
    },
  }).then((result, error) => {
    if (error) return response.status(500).send({ message: error.message });
    return response.status(200).send(true);
  });
};