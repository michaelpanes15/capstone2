let Order = require('../models/Order.js');

module.exports.getAllOrders = (request, response) => {
	return Order.find({}).then((order, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(order == null) return response.status(400).send({status: false, message: "No Orders found"})
		if(order.length <= 0) return response.status(200).send({status: false, message: "No Orders Found"});
		return response.status(200).send({status: true, message: order.reverse()});
	}).catch(error => error.message);
}

module.exports.getAllActiveOrders = (request, response) => {
	return Order.find({orderStatus: "Active"}).then((order, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(order == null) return response.status(400).send({status: false, message: "No Active Orders found"})
		if(order.length <= 0) return response.status(200).send({status: false, message: "No Active Orders Found"});
		return response.status(200).send({status: true, message: order.reverse()});
	})
}

module.exports.getAllPendingOrders = (request, response) => {
	return Order.find({orderStatus: "Pending"}).then((order, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(order == null) return response.status(400).send({status: false, message: "No Pending Orders found"})
		if(order.length <= 0) return response.status(200).send({status: false, message: "No Pending Orders Found"});
		return response.status(200).send({status: true, message: order.reverse()});
	})
}

module.exports.getAllClosedOrders = (request, response) => {
	return Order.find({orderStatus: "Closed"}).then((order, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(order == null) return response.status(400).send({status: false, message: "No Closed Orders found"})
		if(order.length <= 0) return response.status(200).send({status: false, message: "No Closed Orders Found"});
		return response.status(200).send({status: true, message: order.reverse()});
	})
}

module.exports.getAllUserOrders = (request, response) => {
	return Order.find({userId: request.user.id}).then((order, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(order == null) return response.status(400).send({status: false, message: "No Orders found"})
		if(order.length <= 0) return response.status(200).send({status: false, message: "No Orders Found"});
		return response.status(200).send({status: true, message: order.reverse()});
	})
}

module.exports.getAllUserActiveOrders = (request, response) => {
	return Order.find({userId: request.user.id, orderStatus: "Active"}).then((order, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(order == null) return response.status(400).send({status: false, message: "No Active Orders found"})
		if(order.length <= 0) return response.status(200).send({status: false, message: "No Active Orders Found"});
		return response.status(200).send({status: true, message: order.reverse()});
	})
}

module.exports.getAllUserPendingOrders = (request, response) => {
	return Order.find({userId: request.user.id, orderStatus: "Pending"}).then((order, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(order == null) return response.status(400).send({status: false, message: "No Pending Orders found"})
		if(order.length <= 0) return response.status(200).send({status: false, message: "No Pending Orders Found"});
		return response.status(200).send({status: true, message: order.reverse()});
	})
}

module.exports.getAllUserClosedOrders = (request, response) => {
	return Order.find({userId: request.user.id, orderStatus: "Closed"}).then((order, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(order == null) return response.status(400).send({status: false, message: "No Closed Orders found"})
		if(order.length <= 0) return response.status(200).send({status: false, message: "No Closed Orders Found"});
		return response.status(200).send({status: true, message: order.reverse()});
	})
}

module.exports.activateOrder = (request, response) => {
	return Order.findOne({userId: request.user.id, orderStatus: "Pending"}).then((order, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(order == null) return response.status(400).send({status: false, message: "User does not have a Pending order"});

		//Verifies if both inputs are valid
		if((request.body.paymentDetails === "" || request.body.paymentDetails === null || request.body.paymentDetails === undefined) || 
			(request.body.deliveryAddress === "" || request.body.deliveryAddress === null || request.body.deliveryAddress === undefined)) {
			return response.status(400).send({status: false, message: "Please fill up Payment Details and Delivery Address"});
		}
		order.paymentDetails = request.body.paymentDetails;
		order.deliveryAddress = request.body.deliveryAddress;
		order.orderStatus = "Active";
		order.purchasedOn = new Date();
		return order.save().then(result => {
			return response.status(201).send({status: true, message: "Payment Successfull", data: result})
		})
	})
}

module.exports.cancelOrder = (request, response) => {
	return Order.findOne({userId: request.user.id, orderStatus: "Pending", _id: request.body.orderId}).then((order, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(order == null) return response.status(400).send({status: false, message: "User does not have a Pending order"});
		if(order.orderStatus != "Pending") return response.status(400).send({status: false, message: "Order is not Pending"});
		order.orderStatus = "Closed";
		return order.save().then((result, error) => {
			if(error) return response.status(500).send({status: false, message: error.message})
			return response.status(200).send({status: true, message: "Order has successfully been cancelled", data: result})
		})
	})
}

module.exports.closeOrder = (request, response) => {
	return Order.findOne({userId: request.body.userId, orderStatus: "Active", _id: request.body.orderId}).then((order, error) => {
		if(error) return response.status(500).send({message: error.message});
		if(order == null) return response.status(400).send({message: "User does not have an Active order"});
		if(order.orderStatus != "Active") return response.status(400).send({message: "Order is not active"});
		order.orderStatus = "Closed";
		return order.save().then(result => {
			return response.status(201).send({status: true, message: "Order has successfully been closed", data: result})
		})
	})
}