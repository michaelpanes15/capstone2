const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


/*
A Regex function to check if the password input is
1. At least 8 characters
2. Contains at least one uppercase and lowercase letter
3. Contains at least one number
4. Contains at least one of these symbols = @ # $ % ^ & ! 
*/
const isValidPassword = (password) => {
	const pattern = /^(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&!])[A-Za-z\d@#$%^&!]{8,}$/;
	return pattern.test(password);
}

module.exports.registerUser = (request, response) => {
	return User.findOne({email: request.body.email}).then((result, error) => {
		if(error) return response.status(500).send({message: error.message});

		if(result !== null) return response.status(400).send({message: "Email Exists"}); 

		// Uses isValidPassword() to validate a strong password
		// if(!isValidPassword(request.body.password)) return response.status(400).send({status: false, message: "Invalid password. It must have at least 8 characters, an uppercase letter, a number and a symbol"});
		// else {
		// 	let new_user = new User({
		// 		userName: request.body.userName,
		// 		email: request.body.email,
		// 		mobileNo: request.body.mobileNo,
		// 		password: bcrypt.hashSync(request.body.password, 10)
		// 	})
		// 	return new_user.save().then((registered_user, error) => {
		// 		registered_user.password = "";
		// 		return response.status(201).send({
		// 			status: true,
		// 			message: "Successfully registered a user!", 
		// 			data: registered_user
		// 		}).catch(error => {
		// 			return response.status(500).send({status: false, message: error.message})
		// 		});
		// 	}).catch(error => {
		// 		return response.status(500).send({status: false, message: error.message})
		// 	});
		// }

		let new_user = new User({
			userName: request.body.userName,
			email: request.body.email,
			mobileNo: request.body.mobileNo,
			password: bcrypt.hashSync(request.body.password, 10)
		})
		return new_user.save().then((registered_user, error) => {
			registered_user.password = "";
			return response.status(201).send({
				status: true,
				message: "Successfully registered a user!", 
				data: registered_user
			})
		}).catch(error => {
			return response.status(500).send({status: false, message: error.message})
		});
	})
}

module.exports.updateUser = (request, response) => {
	return User.findOne({_id: request.body.id}).then((result, error) => {
		if(error) return response.status(500).send({message: error.message});
		if(result == null) return response.status(400).send({message: "No User Found"});

		// Verifies if the user is the same as the user being updated
		if(result._id != request.user.id) return response.status(400).send({message: "Forbidden Action"});

		// Verifies if the old password input is the same to the user's password
		if(!bcrypt.compareSync(request.body.oldPassword, result.password)) return response.status(400).send({message: "Incorrect Password. Please try again."});
		
		// Verifies if the new password is a strong password
		if(!isValidPassword(request.body.newPassword)) return response.status(400).send({message: "Invalid password. It must have at least 8 characters, an uppercase letter, a number and a symbol"});
		
		// Uses || to prevent a error of user only updating one of the fields below
		result.email = request.body.email || result.email
		result.userName = request.body.userName || result.userName;
		result.password = bcrypt.hashSync(request.body.newPassword, 10); 
		return result.save().then(updated_user => {
			updated_user.password = "";
			if(result != null) return response.status(201).send({message: "User information successfully updated", data: updated_user});
		}).catch(error => console.log(error.message));
	})
}

module.exports.updateProfile = (request, response) => {
	return User.findOne({_id: request.body.id}).then((result, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(result == null) return response.status(400).send({status: false, message: "No User Found"});

		// Verifies if the user is the same as the user being updated
		if(result._id != request.user.id) return response.status(400).send({status: false, message: "Forbidden Action"});
		
		// Uses || to prevent a error of user only updating one of the fields below
		result.mobileNo = request.body.mobileNo || result.mobileNo
		result.userName = request.body.userName || result.userName;
		return result.save().then(updated_user => {
			updated_user.password = "";
			if(result != null) return response.status(201).send({status: true, message: "User information successfully updated", data: updated_user});
		}).catch(error => console.log(error.message));
	})
}

module.exports.resetPassword = (request, response) => {
	return User.findOne({_id: request.body.id}).then((result, error) => {
		if(error) return response.status(500).send({status: false, message: error.message});
		if(result == null) return response.status(400).send({status: false, message: "No User Found"});

		// Verifies if the user is the same as the user being updated
		if(result._id != request.user.id) return response.status(400).send({status: false, message: "Forbidden Action"});

		// Verifies if the old password input is the same to the user's password
		if(!bcrypt.compareSync(request.body.oldPassword, result.password)) return response.status(400).send({status: false, message: "Incorrect Password. Please try again."});
		
		// Uses || to prevent a error of user only updating one of the fields below
		result.password = bcrypt.hashSync(request.body.newPassword, 10); 
		return result.save().then(updated_user => {
			updated_user.password = "";
			if(result != null) return response.status(201).send({status: true, message: "User information successfully updated", data: updated_user});
		}).catch(error => console.log(error.message));
	})
}

module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then((result) => {
		if(result == null) return response.status(400).send("Incorrect email or password");

		// Verifies if the password input is the same to the user's password 
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
		if(isPasswordCorrect) {
			return response.status(200).send({accessToken: auth.createAccessToken(result)})
		}
		else return response.status(400).send({message: "Incorrect email or password"});
	}).catch(error => {return response.status(500).send({message: error})});
}

module.exports.getProfile = (request, response) => {
	return User.findById(request.user.id).then((result, error) => {
		if(error) return response.status(500).send({message: error.message});
		result.password = "";
		return response.status(200).send({message: result});
	})
}

module.exports.getUserProfile = (request, response) => {
	return User.findById(request.params.id).then((result, error) => {
		if(error) return response.status(500).send({message: error.message})
		if(result == null) return response.status(400).send({message: "User not found."});
		result.password = "";
		return response.status(200).send({status: true, message: result});
	})
}

module.exports.getAllProfile = (request, response) => {
	return User.find().then((result, error) => {
		if(error) return response.status(500).send({message: error.message})
		result.forEach((user) => user.password = "");
		return response.status(200).send({status: true, message: result});
	})
}

module.exports.setAdmin = (request, response) => {
	return User.findByIdAndUpdate(request.body.id, {isAdmin: true}, {new: true}).then((result, error) => {
		if(error) return response.status(500).send({message: error.message});
		if(!result) return response.status(400).send({message: "User does not exist"});
		result.password = "";
		return response.status(200).send({message: "User permissions have been updated.", data: result})
	})
}

module.exports.addToCart = async (request, response) => {
	if(request.user.isAdmin) return response.send({status: false, message: 'Action Forbidden'});

	// Verifies if the quantity input is valid 
	// isNaN() - is Not a Number; checks if the input is a string whether it be a number or a string (e.g. 1 and "1" are considered a number)
	if(isNaN(request.body.quantity) || request.body.quantity <= 0 || request.body.quantity === null || request.body.quantity === undefined) return response.status(400).send({message: "Invalid Quantity."})
	
	// Verifies if the request productId is valid 
	let isProductActive = await Product.findById(request.body.productId).then((product, error) => {
		if(error) return {status: false, message: error.message};
		if(product == null || !product.isActive) return {status: false, message: "Product does not exist"};
		return {status: true, message: "Product is active", data: product, } ;
	})
	if(isProductActive.status !== true) return response.status(400).send({status: false, message: isProductActive.message})

	// Verifies if product is already in cart. There can only be one unique product in cart
	// Creates a new product entry to the user.orderCarts
	let isUserUpdated = await User.findById(request.user.id).then(user => {
		let isProductAlreadyInCart = user.orderCarts.find(order => order.productId === request.body.productId);
		if(isProductAlreadyInCart) return {status: false, message: "Product is already in cart."}

		let newProductOrder = {
			productId: request.body.productId,
			name: isProductActive.data.name,
			quantity: request.body.quantity,
			imageUrl: isProductActive.data.imageUrl,
			price: isProductActive.data.price,
			subTotal: isProductActive.data.price * request.body.quantity
		}
		user.orderCarts.push(newProductOrder);
		return user.save().then(updated_cart => true).catch(error => {return {message: error.message}});
	})

	if(isUserUpdated !== true) return response.status(400).send({status: false, message: isUserUpdated});

	if(isUserUpdated) return response.status(201).send({status: true, message: "Product Successfully added in cart"});
}

module.exports.createOrder = async (request, response) => {
	if(request.user.isAdmin) return response.send({status: false, message: 'Action Forbidden'});

	// Verifies if the input have duplicate productIds. 
	let isRequestHaveDuplicates = new Set(request.body.orders).size !== request.body.orders.length;
	if(isRequestHaveDuplicates) return response.status(400).send({message: "The Request has duplicate products. Please ensure only unique IDs are requested."})

	// Verifies if user already have an existing pending order
	let isUserHavePendingOrder = await Order.findOne({userId: request.user.id, orderStatus: "Pending"}).then(result => {
		if(result) return {status: true, message: "User already have pending order!"};
		else return {status: false}
	})
	if(isUserHavePendingOrder.status) return response.status(400).send({status: false, message: isUserHavePendingOrder.message});

	let newOrderList = [];
	let newOrdersListSubtotal = 0;
	let isUserUpdated = await User.findById(request.user.id).then(user => {
		// Verifies if there is a productId input that is not present in the user's cart
		let isCartCleared = request.body.orders.every(productId => {
			let userCart = user.orderCarts.map(product => product.productId);
			return userCart.includes(productId);
		});
		if(isCartCleared){
			// Iterates through each productId
			// Pushes the product to newOrderList and removes cart entry from the user's cart
			// newOrdersListSubtotal = sum of all product subtotals
			request.body.orders.forEach(orderId => {
				// findindex() Returns the index of the orderCart array that contains the productId input
				let cartIndex = user.orderCarts.findIndex(product => product.productId == orderId);
				newOrderList.push(user.orderCarts[cartIndex]);
				newOrdersListSubtotal += user.orderCarts[cartIndex].subTotal;
				user.orderCarts.splice(cartIndex, 1);
			});
		}
		else return {code: 400, status: false, message: "One or more Products not in cart"}
		return user.save().then(updated_cart =>{
			return {status: true}
		}).catch(error => { return {code: 500, status: false, message: error.message}});
	});
	if(isUserUpdated.status !== true) return response.status(isUserUpdated.code).send({status: isUserUpdated.status, message: isUserUpdated.message});

	let new_order = new Order({
		userId: request.user.id,
		products: newOrderList,
		totalAmount: newOrdersListSubtotal
	});

	return new_order.save().then(registered_order => {
		return response.status(201).send({status: true, message: "Successfully created order!", data: registered_order})
	}).catch(error => error.message);
}

module.exports.getCart = (request, response) => {
	return User.findById(request.user.id).then((user, error) => {
		if(error) return response.send({status: false, message: error.message});
		//if(user.orderCarts.length == 0) return response.status(400).send({status: false, message: "User has no products in cart"});
		return response.status(200).send({status: true, userId: request.user.id, data: user.orderCarts});
	})
}

module.exports.editCart = (request, response) => {
	return User.findById(request.user.id).then(user => {
		if(user.orderCarts.length <= 0) return response.status(400).send({message: "User has no item in cart"})

		// Verifies if the quantity input is valid 
		// isNaN() - is Not a Number; checks if the input is a string whether it be a number or a string (e.g. 1 and "1" are considered a number)
		if(isNaN(request.body.quantity) || request.body.quantity <= 0 || request.body.quantity === null || request.body.quantity === undefined) return response.status(400).send({message: "Invalid Quantity."})

		let productIndex = user.orderCarts.findIndex(product => product.productId === request.body.id);
		user.orderCarts[productIndex] = {
			productId: user.orderCarts[productIndex].productId,
			name: user.orderCarts[productIndex].name,
			subTotal: (user.orderCarts[productIndex].subTotal/user.orderCarts[productIndex].quantity) * request.body.quantity,
			quantity: request.body.quantity
		}
		return user.save().then(updated_cart => {
			return response.status(200).send({message: "Cart has successfully been updated", data: user.orderCarts[productIndex]});
		}).catch(error => response.status(500).send({message: error.message}));
	})
}

module.exports.removeCart = (request, response) => {
	return User.findById(request.user.id).then(user => {
		if(user.orderCarts.length <= 0) return response.status(400).send({status: false, message: "User has no item in cart"})
		let productIndex = user.orderCarts.findIndex(product => product.productId == request.params.id);
		user.orderCarts.splice(productIndex, 1);
		return user.save().then(updated_cart => {
			return response.status(200).send({status: true, message: "Item has successfully been removed from cart", data: updated_cart.orderCarts});
		}).catch(error => response.status(500).send({status: false, message: error.message}));
	})
}

module.exports.clearCart = (request, response) => {
	return User.findById(request.user.id).then(user => {
		if(user.orderCarts.length <= 0) return response.status(400).send({message: "User has no item in cart"})
		user.orderCarts = [];
		return user.save().then(updated_cart => {
			return response.status(200).send({message: "Cart has successfully been cleared", data: updated_cart.orderCarts});
		}).catch(error => response.status(500).send({message: error.message}));
	})
}

